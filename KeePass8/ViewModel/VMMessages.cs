﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePass8.Model;
using KeePassLib.Serialization;

namespace KeePass8.ViewModel
{
    /// <summary>
    /// Copy the content to the clipboard
    /// </summary>
    public class ClipboardMessage
    {
        public string ContentToCopyToClipboard;
    }

    /// <summary>
    /// Asynchroneous open file (including file picking)
    /// </summary>
    public class OpenFileMessage
    {
        // Call parameters
        public bool isLocalFile;

        // Returned values
        public bool Success;
    }   
    
    /// <summary>
    /// A file has been picked.  
    /// </summary>
    public class FilePickedMessage
    {
        // Call parameters
         public IOConnectionInfo IOConnectionInfo;
    }

    public class UserAuthentificationDialogMessage
    {
        // Call parameters:
        public IOConnectionInfo IOConnectionInfo;

        // Callback values:
        public bool Success;
        public IDatabaseModel Model;
    }

}
