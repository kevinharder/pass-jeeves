﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using KeePass8.Model;
using KeePassLib.Serialization;

namespace KeePass8.ViewModel.Pages.OpenFile
{
    public class OpenFileRecentVM : ViewModelBase
    {
        private IApplicationModel _model;

        public OpenFileRecentVM(IApplicationModel model)
        {
            _model = model;
            RecentFiles = new ObservableCollection<IKeePassFileInfo>();
            PopulateFileInfo();
        }

        private async Task PopulateFileInfo()
        {
            foreach (IOConnectionInfo ioc in _model.RecentFiles)
            {
                IKeePassFileInfo info;
#if DEBUG
                if (ViewModelBase.IsInDesignModeStatic)
                    info = new KeePassFileInfoDesignData();
                else
#endif
                    info = new KeePassFileInfo(ioc);
                
                RecentFiles.Add(info);
            }
            foreach (KeePassFileInfo info in RecentFiles)
            {
                await info.UpdateInfo();
            }
        }

        public ObservableCollection<IKeePassFileInfo> RecentFiles { get; private set; }

        public IKeePassFileInfo SelectedFile
        {
            get { return null; }

            set
            {
                Messenger.Default.Send((new NotificationMessage<FilePickedMessage>(
                    new FilePickedMessage { IOConnectionInfo = value.IOConnection },
                    "Recent file picked")));
            }
        }
    }
}
