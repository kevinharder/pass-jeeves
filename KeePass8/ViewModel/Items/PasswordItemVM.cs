﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePassLib;
using Windows.UI.Xaml.Media;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using KeePass8.Model;

namespace KeePass8.ViewModel
{

    /// <summary>
    /// A display item representing a password
    /// </summary>
    /// 
    public class PasswordItemVM
    {
        private IconBitmap IconImage { get; set; }

        public PasswordItemVM(PwEntry pwEntry)
        {
            m_pwEntry = pwEntry;
            Title = pwEntry.Strings.GetSafe(PwDefs.TitleField).ReadString();
            IconImage = new IconBitmap(pwEntry.IconId);
            UserName = pwEntry.Strings.GetSafe(PwDefs.UserNameField).ReadString();
            URL = pwEntry.Strings.GetSafe(PwDefs.UrlField).ReadString();
            Notes = pwEntry.Strings.GetSafe(PwDefs.NotesField).ReadString();
            CopyPasswordToClipboardCommand = new RelayCommand(() => CopyPasswordToClipboard());
            CopyUserNameToClipboardCommand = new RelayCommand(() => CopyUserNameToClipboard());
        }

        public string Title { get; set; }
        public string UserName { get; set; }
        public string URL { get; set; }
        public string Notes { get; set; }

        public ImageSource Image
        {
            get { return IconImage.Image; }
        }

        private PwEntry m_pwEntry;
        public PwEntry PwEntry
        {
            get { return m_pwEntry; }
        }


        public RelayCommand CopyPasswordToClipboardCommand
        {
            get;
            private set;
        }

        private void CopyPasswordToClipboard ()
        {
            Messenger.Default.Send<ClipboardMessage>(new ClipboardMessage { ContentToCopyToClipboard = m_pwEntry.Strings.GetSafe(PwDefs.PasswordField).ReadString() });
        }

        public RelayCommand CopyUserNameToClipboardCommand
        {
            get;
            private set;
        }

        private void CopyUserNameToClipboard()
        {
            Messenger.Default.Send<ClipboardMessage>(new ClipboardMessage { ContentToCopyToClipboard = UserName });
        }

    }

}

