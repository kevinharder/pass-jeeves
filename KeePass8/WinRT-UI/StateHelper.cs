﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace KeePass8
{
    public class StateHelper
    {
        public static readonly DependencyProperty StateProperty = DependencyProperty.RegisterAttached(
            "State",
            typeof(String),
            typeof(StateHelper),
            new PropertyMetadata(null, StateChanged));

        public static void StateChanged(DependencyObject target, DependencyPropertyChangedEventArgs args)
        {
            string newValue = args.NewValue as string;
            Control targetControl = target as Control;

            if ( (newValue != null) && (targetControl != null))
                VisualStateManager.GoToState((Control)target, newValue, true);
        }

        public static void SetState(DependencyObject obj, string value)
        {
            obj.SetValue(StateProperty, value);
        }
        
        public static string GetState(DependencyObject obj)
        {
            return (string) obj.GetValue(StateProperty);
        }

    }

}
